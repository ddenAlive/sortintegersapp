package com.dden.task;

import java.util.Arrays;

/**
 * The SortIntegers program implements an application that
 * takes up to ten command-line arguments as integer values,
 * sorts them in the ascending order, and then prints them into standard output.
 *
 * @author Denys Parshutkin
 * @version 1.0
 * @since 2023-08-06
 */
public class SortIntegers {

    public static void main(String[] args) {
        SortIntegers sortIntegers = new SortIntegers();
        sortIntegers.sortAndPrint(args);
    }

    public void sortAndPrint(String[] args) {
        if (args.length == 0) {
            System.out.println("No arguments provided.");
            return;
        }

        int[] numbers = new int[args.length];

        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                System.out.println("Invalid argument: " + args[i]);
                return;
            }
        }

        Arrays.sort(numbers);

        System.out.print("Sorted numbers: ");
        for (int number : numbers) {
            System.out.print(number + " ");
        }
    }
}
