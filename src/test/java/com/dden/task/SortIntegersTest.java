package com.dden.task;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortIntegersTest {
    private String[] input;
    private String expectedOutput;

    public SortIntegersTest(String[] input, String expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"5", "2", "8", "1", "9"}, "Sorted numbers: 1 2 5 8 9"},
                {new String[]{"3", "7", "2", "6"}, "Sorted numbers: 2 3 6 7"},
                {new String[]{"10", "9", "8", "7", "6", "5", "4", "3", "2", "1"}, "Sorted numbers: 1 2 3 4 5 6 7 8 9 10"},
                {new String[]{"0"}, "Sorted numbers: 0"},
        });
    }
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testSortAndPrint() {
        SortIntegers app = new SortIntegers();
        app.sortAndPrint(input);

        String actualOutput = outContent.toString().trim();
        assertEquals(expectedOutput, actualOutput);


    }
}
